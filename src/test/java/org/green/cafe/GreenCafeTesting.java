package org.green.cafe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.RestAssured;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.green.cafe.controllers.AuthController;
import org.green.cafe.controllers.UserController;
import org.green.cafe.exceptions.ValidationException;
import org.green.cafe.generators.DisplayNameGenerated;
import org.green.cafe.models.User;
import org.green.cafe.models.dto.requests.LoginRequest;
import org.green.cafe.models.dto.requests.UserRequest;
import static org.junit.jupiter.api.Assertions.*;

import org.green.cafe.models.dto.responses.LoginResponse;
import org.green.cafe.services.UserService;
import org.green.cafe.util.JwtUtil;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.Mockito;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;

@DisplayNameGeneration(value = DisplayNameGenerated.class)
@QuarkusTest
@Tag("Green-Cafe-Project")
@DisplayName("Green-Cafe-Project")
@Execution(ExecutionMode.CONCURRENT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ActivateRequestContext
class GreenCafeTesting {

  @Inject
  JsonWebToken jsonWebToken;

  @Inject
  private UserService userService;

  @Inject
  private UserController userController;

  @Inject
  AuthController authController;

  private UserRequest userRequest;
  private LoginRequest loginRequest;
  private LoginResponse loginResponse;

  ObjectMapper objectMapper;


  @BeforeAll
  void setUp() {
    userRequest = new UserRequest();
    userRequest.fullName = "haryaugust";
    userRequest.email = "tech.hary.agustian@gmail.com";
    userRequest.mobilePhoneNumber = "089601902205";
    userRequest.workPhoneNumber = "0215462465";
    userRequest.loginName = "haryagustian08";
    userRequest.password = BcryptUtil.bcryptHash("Sangatrahasia17");
    userRequest.address = "Jl.Industri Kapal Dalam No. 14 RT.006 RW.011 Tugu Cimanggis Depok, Kode POS 16451.";
  }


  @Test
  @Order(1)
  @Timeout(value = 5, unit = TimeUnit.SECONDS)
  void whenPostSuccess() throws InterruptedException {
    Response result = userController.post(userRequest);
    assertEquals(Response.Status.CREATED.getStatusCode(), result.getStatus());
    assertNotNull(User.listAll().get(0));
    assertEquals(User.count(), 1);
    assertEquals(new HashMap<>(), result.getEntity());
    assertNotEquals(User.count(), 0);
    Thread.sleep(1_000);
  }

  @Test
  @Order(2)
  void whenPostFailed() throws InterruptedException {
    assertThrows(ValidationException.class, () -> {
      userController.post(userRequest);
    });
    assertThrowsExactly(ValidationException.class,() -> {
      assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), userController.post(userRequest).getStatus());
      assertEquals("LOGIN_NAME_IS_EXISTS", userController.post(userRequest).getEntity());
      assertEquals("INVALID_PASSWORD", userController.post(userRequest).getEntity());
      assertEquals("INVALID_REQUEST", userController.post(userRequest).getEntity());
    });
    assertNotEquals(User.count(), 0);
    Thread.sleep(1_000);
  }

  @Test
  @Order(5)
  @TestSecurity(user = "userPut", roles = "user")
  @JwtSecurity(claims = {
      @Claim(key = "fullName", value = "haryaugust"),
      @Claim(key = "email", value = "tech.hary.agustian@gmail.com"),
      @Claim(key = "mobilePhoneNumber", value = "089601902205"),
      @Claim(key = "workPhoneNumber", value = "0215462465"),
      @Claim(key = "address", value = "Jl.Industri Kapal Dalam No. 14 RT.006 RW.011 Tugu Cimanggis Depok, Kode POS 16451.")
  })
  @Disabled
  void whenPutSuccess() throws InterruptedException {

    Optional<User> optionalUser = User.findByLoginName("haryagustian08");

    User userUpdate = optionalUser.get();

    userUpdate.setFullName("bhullsbhulls");
    userUpdate.setEmail("tech.hary.agustian@gmail.com");
    userUpdate.setMobilePhoneNumber("089601902205");
    userUpdate.setWorkPhoneNumber("0215462465");
    userUpdate.setLoginName("haryagustian08");
    userUpdate.setPassword(BcryptUtil.bcryptHash("Sangatrahasia17"));
    userUpdate.setAddress("Jl.Industri Kapal Dalam No. 14 RT.006 RW.011 Tugu Cimanggis Depok, Kode POS 16451.");

    Response response = userService.put(jsonWebToken.getClaim("id").toString(), userRequest);
    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());


    Thread.sleep(1_000);
  }

  @Test
  @Order(3)
  void whenloginSuccess() throws JsonProcessingException, InterruptedException {
    User user = User.findByLoginName("haryagustian08").get();

    loginRequest = new LoginRequest();
    loginRequest.loginName = userRequest.loginName;
    loginRequest.password = userRequest.password;

    loginResponse = new LoginResponse();
    loginResponse.user = user;
    loginResponse.token = JwtUtil.generateToken(user);

    Response response = authController.login(loginRequest);

    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    JsonObject jsonObject = new JsonObject(objectMapper.writeValueAsString(response.getEntity()));

    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    assertTrue(jsonObject.containsKey("user"));
    assertTrue(jsonObject.containsKey("token"));

    Thread.sleep(1_000);
  }

  @Test
  @Order(4)
  void whenloginFailed() throws InterruptedException {
    loginRequest = new LoginRequest();
    loginRequest.loginName = "username";
    loginRequest.password = "password";
    assertThrowsExactly(ValidationException.class,() -> {
      assertEquals("INVALID_PASSWORD", authController.login(loginRequest).getEntity());
      assertEquals("USER_NOT_FOUND", authController.login(loginRequest).getEntity());
      assertEquals("WRONG_PASSWORD", authController.login(loginRequest).getEntity());
    });
    Thread.sleep(1_000);
  }
}
