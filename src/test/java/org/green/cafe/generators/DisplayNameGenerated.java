package org.green.cafe.generators;

import java.lang.reflect.Method;
import java.util.Arrays;

public class DisplayNameGenerated implements org.junit.jupiter.api.DisplayNameGenerator {
  @Override
  public String generateDisplayNameForClass(Class<?> aClass) {
    return "Test " + aClass.getSimpleName();
  }

  @Override
  public String generateDisplayNameForNestedClass(Class<?> aClass) {
    return "Test " + aClass.getSimpleName();
  }

  @Override
  public String generateDisplayNameForMethod(Class<?> aClass, Method method) {
    return "Test " + aClass.getSimpleName() + " " + method.getName();
  }
}
